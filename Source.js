class Source {
  constructor(root) {
    this.root = new Node(root)
    this.nodes = []
  }

  add(parent, node) {
    this.nodes.push(new Node(node, [parent]))
  }

  static parse(sourceCode, name, mode = "simple") {
    let tree = new Source(name)

    if(mode == "simple") {
      let lines = sourceCode.trim().split("\n")
      lines.forEach((line, ln) => {
        tree.add(tree.root, { line: line, ln: ln })
      });

      return tree
    }

    return tree
  }

  toString() {
    console.log(`Root: ${this.root}\nNodes: ${this.nodes.length}`)
  }

  async toIPFS(ipfs) {
    ipfs.object.put({ Data: tree.root.data, Links: [] })
  }
}

class Node {
  constructor(data, parents = []) {
    this.data = data
    this.parents = parents
  }

  encode() {
    return {
      data: data,
      parents: parents
    }
  }
}

module.exports = Source;
