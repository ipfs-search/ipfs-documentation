const Source = require("./Source")

test("Parse source code simply", () => {
  let sourceCode = `function foobar(x) {
    if(x % 15 == 0) {
      return "foobar"
    } else if(x % 5 == 0) {
      return "bar"
    } else if(x % 3 == 0) {
      return "foo"
    } else {
      return ""
    }
  }`
  let name = "main.js"

  let tree = Source.parse(sourceCode, name)

  expect(tree.nodes.length).toBe(sourceCode.trim().split("\n").length)
  expect(tree.root.data).toBe(name)
})
